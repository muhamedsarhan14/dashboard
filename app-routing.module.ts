import { ExtraOptions, RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { QRCodeComponent } from "angularx-qrcode";
import { AuthenticationComponent } from './authentication/pages.component';
import { LoginComponent } from './authentication/login/login.component';
import { AuthGuardService } from './shared/guards/auth-guard.service';

const routes: Routes = [
  { path: "pages", loadChildren: "app/pages/pages.module#PagesModule",canActivate:[AuthGuardService]},
  {
  path: "auth",
  component: AuthenticationComponent,
  children: [
    {
      path: "login",
      component: LoginComponent,
    }
  ],
  },

  { path: "", redirectTo: "auth/login", pathMatch: "full" },
  { path: "**", redirectTo: "pages" }
];

const config: ExtraOptions = {
  useHash: false
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
