import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// import { PagesComponent } from "./pages.component";
// import { DashboardComponent } from "./dashboard/dashboard.component";
// import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";
// import { QrComponent } from "./qr/qr.component";
// import { UserBalanceComponent } from "./user-balance/user-balance.component";
// import { SearchResultComponent } from "./search-result/search-result.component";
import { NbLayoutModule } from '@nebular/theme';
// import { NbLoginComponent, NbRegisterComponent } from '@nebular/auth';
import { LoginComponent } from './login/login.component';
import { AuthenticationComponent } from './pages.component';
// import { AuthenticationComponent } from './pages.component';
// import { AuthenticationComponents } from './pages.component';
// import { EditProfileComponent } from './edit-profile/edit-profile.component';
// import { DeactivateComponent } from './deactivate/deactivate.component';

const routes: Routes = [
  {
    path: "auth",
    component: AuthenticationComponent,
    children: [
      {
        path: "login",
        component: LoginComponent,
        pathMatch: "full"
      },
     
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    NbLayoutModule
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
